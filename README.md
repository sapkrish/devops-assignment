# DevOps Assignment

This repository is a skeleton for the assignment for DevOps candidates.

### Goal
The goal of this assignment is to use a Bitbucket Pipeline to successfully package a given
Spring Boot Maven project and deploy it to Google Kubernetes Engine. 

Once deployed, the Kubernetes instance should show an index page with “Hello world, I’m
running on Production”, where ‘Production’ is an app property set in the respective
configuration file. 

At the end we should receive a Pull Request and access to the GCP project. We will check
the bitbucket-pipelines.yaml file and any other scripts needed to run the pipeline.

### How to use?
Fork the master branch and clone the repo to develop locally. You will also have to have a Google Cloud Project.

### The Bitbucket Pipeline 
The script should deploy a JAR package of the project to three different environments
(Develop, QA, and Production) on a Kubernetes Engine instance. 

The pipeline script can make use of a custom Docker Image, but this is not mandatory. 

For Bitbucket to be able to deploy to Google Cloud you will have to create a Service
Account in the Google Cloud project.

The deployment script has to dynamically create all the necessary Cloud services if they are absent when the pipeline runs. 

### Installing 
This project uses Java 8 SDK, Spring Boot, and Maven.
 
In IntelliJ, to specify which profile to use (develop/qa/prod), set 
 
 ```-Dspring.profiles.active=(develop/qa/production)```
 
 as the VM option in the Application's Run/Debug configurations. 
 
Run ```$ mvn install``` to build the project.

### Tests
Run `` $ mvn test `` to run the tests.

To test the output of the ``index.html`` file, go to [localhost](localhost:8080).

### Deploying

One of the goals of the assignment is to set up the bitbucket-pipelines.yaml file, to deploy the app to Kubernetes.

#### Who do I talk to?
 CS Digital Media 
