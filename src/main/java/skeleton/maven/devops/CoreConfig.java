package skeleton.maven.devops;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
public class CoreConfig {

  @Bean
  public ConfigurableServletWebServerFactory containerCustomizer() {
    TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
    return factory;
  }

}
