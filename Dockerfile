FROM openjdk:8
COPY target/demo-0.0.1-SNAPSHOT.jar demoapp.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "demoapp.jar"]